# Site da Comunidade Software Livre em Belo Horizonte

Site para a Comunidade Software Livre em Belo Horizonte.

## Contato

* [Grupo no telegram](https://t.me/bhlivre)
* [Sala na rede Matrix](https://matrix.to/#/#bhlivre:kde.org)

## Eventos

* [FLISOL 2024 e MiniDebConf Belo Horizonte](https://bh.mini.debconf.org/) - 27 a 30 de abril na UFMG
* [SFD 2023](https://digitalfreedoms.org/en/software-freedom-day/events/sfd-2023-belohorizonte-brazil) - 30 de setembro no CEFET-MG
* [Debian Day 2023](https://debian-minas-gerais.gitlab.io/site/) - 12 de agosto de 2023 no espaço do conhecimento da UFMG
* [FLISOL 2023](https://flisol.info/FLISOL2023/Brasil/BeloHorizonte) - 15 de abril no ICEx UFMG

## Fotos de eventos

<https://softwarelivre.bhz.br/fotos>

## Repositório

O código fonte desse site e os arquivos usados nos eventos estão disponíveis em: <https://gitlab.com/bhlivre/softwarelivre.bhz.br>
