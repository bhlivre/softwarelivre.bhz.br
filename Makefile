all: public/index.html public/assista.png public/style.css

public/index.html public/assista.png: public

public:
	mkdir -p public

public/index.html: README.md style.css
	pandoc \
		--metadata pagetitle='Software Livre em Minas Gerais' \
		--standalone \
		--from=markdown \
		--to=html \
		--css=style.css \
		--output=$@ \
		README.md


#public/assista.png: assista.png
#	cp $< $@

public/style.css: style.css
	cp $< $@

clean:
	$(RM) -r public
